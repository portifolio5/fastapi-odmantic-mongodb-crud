from typing import Optional
from pydantic import BaseModel


class PlayerSchema(BaseModel):
    password: str
    name: str
    game: str

    class Config:
        schema_extra = {
            "example": {
                "password": "steam123",
                "name": "thiago",
                "game": "battlefield 1",
            }
        }

class PlayerOutSchema(BaseModel):
    name: str
    game: str


class PlayerPatchSchema(BaseModel):
    name: Optional[str]
    game: Optional[str]