import hashlib  # Biblioteca de "hasheamento" nativa do python
from hmac import compare_digest  # Função de comparação entre hashes

"""
# mb5: Função de hasheamento usando a técnica md5, há outras opções como sha256(), blake2b()
e blake2s() essas funções só aceitam strings no formato de "bytes", você pode usar a classe "bytes"
passando como primeiro argumento sua string e como segundo um encode utf-8. Você também
pode passar uma string fixa com um "b" na frente dela, ex: b"dark souls"

# hexdigest: Função de que retorna uma verção "digerida" dos dados em formato de string,
caso o output desejado seja em bytes, adicione um ponto e invoque a função encode("utf-8"),
você tbm pode usar somente digest(), já que a versão "hex" é em hexadecimal.

# compare_digest: compara dois dados hasheados, a comparação é somente aceita entre dados do mesmo
tipo.
"""


def hashing_password(password: str):
    encrypted_password = hashlib.md5(bytes(password, "utf-8"))
    return encrypted_password.hexdigest()


# def compare_password(password: str, hashed_password: str):
#     encrypted_password = hashlib.md5(bytes(password, "utf-8")).hexdigest()
#     isTheSame = compare_digest(encrypted_password, hashed_password)
#     return isTheSame