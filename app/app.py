from fastapi import FastAPI, HTTPException
from odmantic import ObjectId
from app.database import engine
from app.schemas.player import *
from app.server.CRUD import *


app = FastAPI()


@app.get("/user/get_all")
async def resgatar_todos_os_jogadores():
    users = await get_all_users()
    if users:
        return users
    else:
        raise HTTPException(status_code=404, detail="Usuários não encontrados.")


@app.get("/user/get_user_by_password", response_model=PlayerOutSchema)
async def resgatar_jogadores_pela_senha(password: str):
    user = await search_user_by_password(password)
    if user:
        return user
    else:
        raise HTTPException(status_code=404, detail="Usuário não encontrado.")


@app.post("/user/post_player", response_model=PlayerOutSchema)
async def cadastrar_jogador(player: PlayerSchema):
    user = await add_user(player)
    if user:
        return user
    else:
        raise HTTPException(status_code=500, detail="Usuário não adicionado.")


@app.patch("/user/patch_player")
async def atualizar_jogador(password: str, nome: str):
    user = await search_user_by_password(password)
    if user is None:
        raise HTTPException(status_code=404, detail="Usuário não encontrado.")
    else:
        setattr(user, "name", nome)
        await engine.save(user)
        return user


@app.delete("/user/delete_player")
async def deletar_jogador_pela_senha(password: str):
    user = await search_user_by_password(password)
    if user is None:
        raise HTTPException(status_code=404, detail="Usuário não encontrado.")
    else:
        await engine.delete(user)
        return {"detail": "Usuário deletado com sucesso."}
