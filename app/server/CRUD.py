from app.models.player import *  # Modelos que descrevem o banco de dados
from app.schemas.player import *  # Schemas que funcionan como compro de requisição
from app.helpers.helper import hashing_password  # Função de "hasheamento" de senha
from app.database import engine  # "Motor" de comunicação com o banco de dados


async def add_user(player: PlayerSchema):
    setattr(player, "password", hashing_password(player.password))
    user = await engine.save(
        Player(password=player.password, name=player.name, game=player.game)
    )
    return user


async def search_user_by_password(password: str):
    hashed_password = hashing_password(password)
    user = await engine.find_one(Player, Player.password == hashed_password)
    return user


async def get_all_users():
    user = await engine.find(Player)
    return user
