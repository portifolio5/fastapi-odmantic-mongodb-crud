from odmantic import Model, Field
from typing import Optional


class Player(Model):
    password: str = Field(primary_field=True)
    name: str
    game: Optional[str] = None